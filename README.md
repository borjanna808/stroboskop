# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:borjanna808/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/borjanna808/stroboskop/commits/280d06e9127b8cdda7528d92e9458c4016080b73

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/borjanna808/stroboskop/commits/bf96ef365a466c0e3744600aa83300909dffd98c

Naloga 6.3.2:
https://bitbucket.org/borjanna808/stroboskop/commits/ff56694294a7bba353fd66db0ca3ca2f163cd492

Naloga 6.3.3:
https://bitbucket.org/borjanna808/stroboskop/commits/4e748c61bfbc439cd88c0a1be50431a219e7ed4c

Naloga 6.3.4:
https://bitbucket.org/borjanna808/stroboskop/commits/d2e78541c58ba53bef30fa201eb0431898526c5b

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/borjanna808/stroboskop/commits/508bd3176756badcd827c35e46ce94fa48e8e9c9

Naloga 6.4.2:
https://bitbucket.org/borjanna808/stroboskop/commits/66c7e409ab2c587a4f2ca18064d78d8b840f299c

Naloga 6.4.3:
https://bitbucket.org/borjanna808/stroboskop/commits/1b53e8374265bc37ad80a1071503c247c55826d0

Naloga 6.4.4:
https://bitbucket.org/borjanna808/stroboskop/commits/6beb44cfb8d8f9b95163ea3044ff217ac6eebad6